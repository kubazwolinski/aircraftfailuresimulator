﻿using SimConnectService;
using SimulationController.Failures;
using SimulationController.SimEvents;
using SimulationController.SimVariables;
using System;

namespace SimulationController
{
    public class SimulationController
    {
        private SimConnectWrapper wrapper;

        public SimulationController()
        {
            wrapper = new SimConnectWrapper();
            FailureFactory = new FailureFactory()
            {
                SimEventController = new SimEventController(wrapper),
                SimVariableController = new SimVariableController(wrapper)
            };
            PauseController = new PauseController(FailureFactory.SimEventController);
        }

        public void connect(IntPtr handle)
        {
            wrapper.connect(Name, handle);
        }

        public void disconnect()
        {
            wrapper.disconnect();
        }

        public string Name { get; set; } = "SimConnect";
        public PauseController PauseController { get; private set; }
        public FailureFactory FailureFactory { get; private set; }
    }
}
