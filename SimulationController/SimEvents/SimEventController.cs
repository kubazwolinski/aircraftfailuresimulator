﻿using SimConnectService;
using SimulationController.SimEvents;

namespace SimulationController.SimEvents
{
    public class SimEventController
    {
        enum GROUP_IDS
        {
            GROUP_0,
        }

        private readonly SimConnectWrapper wrapper;

        public SimEventController(SimConnectWrapper wrapper)
        {
            this.wrapper = wrapper;
        }

        public void RegisterEvent(SimEvent simEvent)
        {
            wrapper.mapClientToSimEvent(simEvent.SimEventEnum, simEvent.EventID);
        }

        public void Set(SimEvent simEvent, bool value)
        {
            wrapper.transmitClientEvent(simEvent.SimEventEnum, (uint)(value ? 1 : 0), GROUP_IDS.GROUP_0);
        }

    }
}
