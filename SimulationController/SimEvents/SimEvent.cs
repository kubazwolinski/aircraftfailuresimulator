﻿namespace SimulationController.SimEvents
{
    public class SimEvent
    {
        public SimEventEnum SimEventEnum { get; private set; }

        public string EventID { get { return SimEventEnum.ToString(); } }

        public SimEvent(SimEventEnum simEventEnum)
        {
            SimEventEnum = simEventEnum;
        }
    }
}
