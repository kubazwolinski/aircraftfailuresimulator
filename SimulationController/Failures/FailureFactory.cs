﻿using SimulationController.Failures.AircraftFailures;
using SimulationController.Failures.PanelFailures;
using SimulationController.SimEvents;
using SimulationController.SimVariables;

namespace SimulationController.Failures
{
    public class FailureFactory
    {
        public SimEventController SimEventController { get; set; }
        public SimVariableController SimVariableController { get; set; }

        public Failure Create(SimEventEnum simEventEnum)
        {            
            var failure = new AircraftFailure(SimEventController, new SimEvent(simEventEnum));
            failure.Register();
            return failure;
        }

        public Failure Create(SimVariableEnum simVariableEnum)
        {
            var failure = new PanelFailure(SimVariableController, new SimVariable(simVariableEnum));
            failure.Register();
            return failure;
        }
    }
}
