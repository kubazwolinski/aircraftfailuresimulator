﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimulationController.Failures
{
    public interface Failure
    {
        void Set(bool value);

        void Register();
    }
}
