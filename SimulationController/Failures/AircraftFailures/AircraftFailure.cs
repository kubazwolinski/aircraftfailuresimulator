﻿using SimulationController.SimEvents;

namespace SimulationController.Failures.AircraftFailures
{
    public class AircraftFailure : Failure
    {
        private readonly SimEvent simEvent;

        private readonly SimEventController simEventController;

        public AircraftFailure(SimEventController simEventController, SimEvent simEvent)
        {
            this.simEvent = simEvent;
            this.simEventController = simEventController;
        }

        public void Set(bool value)
        {
            simEventController.Set(simEvent, value);
        }

        public void Register()
        {
            simEventController.RegisterEvent(simEvent);
        }
    }
}
