﻿using SimulationController.SimEvents;

namespace SimulationController.Failures.AircraftFailures
{
    #region Basic Failures

    public class VacuumFailure : AircraftFailure
    {
        public VacuumFailure(SimEventController simEventController)
            : base(simEventController, new SimEvent(SimEventEnum.TOGGLE_TOTAL_BRAKE_FAILURE))
        {
        }
    }

    public class ElectricalFailure : AircraftFailure
    {
        public ElectricalFailure(SimEventController simEventController)
            : base(simEventController, new SimEvent(SimEventEnum.TOGGLE_ELECTRICAL_FAILURE))
        {
        }
    }


    public class PitotBlockage: AircraftFailure
    {
        public PitotBlockage(SimEventController simEventController)
            : base(simEventController, new SimEvent(SimEventEnum.TOGGLE_PITOT_BLOCKAGE))
        {
        }
    }

    public class StaticPortBlockage : AircraftFailure
    {
        public StaticPortBlockage(SimEventController simEventController)
            : base(simEventController, new SimEvent(SimEventEnum.TOGGLE_STATIC_PORT_BLOCKAGE))
        {
        }
    }


    public class HydraulicFailure : AircraftFailure
    {
        public HydraulicFailure(SimEventController simEventController)
            : base(simEventController, new SimEvent(SimEventEnum.TOGGLE_HYDRAULIC_FAILURE))
        {
        }
    }

    #endregion

    #region Brake Failures
    public class TotalBrakeFailure : AircraftFailure
    {
        public TotalBrakeFailure(SimEventController simEventController)
            : base(simEventController, new SimEvent(SimEventEnum.TOGGLE_TOTAL_BRAKE_FAILURE))
        {
        }
    }

    public class LeftBrakeFailure : AircraftFailure
    {
        public LeftBrakeFailure(SimEventController simEventController)
            : base(simEventController, new SimEvent(SimEventEnum.TOGGLE_LEFT_BRAKE_FAILURE))
        {
        }
    }

    public class RightBrakeFailure : AircraftFailure
    {
        public RightBrakeFailure(SimEventController simEventController)
            : base(simEventController, new SimEvent(SimEventEnum.TOGGLE_RIGHT_BRAKE_FAILURE))
        {
        }
    }
    #endregion

    #region Engine Failures
    public class Engine1Failure : AircraftFailure
    {
        public Engine1Failure(SimEventController simEventController)
            : base(simEventController, new SimEvent(SimEventEnum.TOGGLE_ENGINE1_FAILURE))
        {
        }
    }

    public class Engine2Failure : AircraftFailure
    {
        public Engine2Failure(SimEventController simEventController)
            : base(simEventController, new SimEvent(SimEventEnum.TOGGLE_ENGINE2_FAILURE))
        {
        }
    }

    public class Engine3Failure : AircraftFailure
    {
        public Engine3Failure(SimEventController simEventController)
            : base(simEventController, new SimEvent(SimEventEnum.TOGGLE_ENGINE3_FAILURE))
        {
        }
    }

    public class Engine4Failure : AircraftFailure
    {
        public Engine4Failure(SimEventController simEventController)
            : base(simEventController, new SimEvent(SimEventEnum.TOGGLE_ENGINE4_FAILURE))
        {
        }
    }

    #endregion
}
