﻿using SimulationController.SimVariables;

namespace SimulationController.Failures.PanelFailures
{
    public class PanelFailure : Failure
    {
        private readonly SimVariable simVariable;

        private readonly SimVariableController simVariableController;

        public PanelFailure(SimVariableController simVariableController, SimVariable simVariable)
        {
            this.simVariable = simVariable;
            this.simVariableController = simVariableController;
        }

        public void Set(bool value)
        {
            simVariableController.Set(simVariable, value);
        }

        public void Register()
        {
            simVariableController.RegisterVariable(simVariable);
        }
    }
}
