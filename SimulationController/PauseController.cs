﻿using SimulationController.SimEvents;

namespace SimulationController
{
    public class PauseController
    {
        private readonly SimEvent simEvent = new SimEvent(SimEventEnum.PAUSE_SET);

        private readonly SimEventController simEventController;

        public PauseController(SimEventController simEventController)
        {
            this.simEventController = simEventController;
        }

        public void PauseOn()
        {
            set(true);
        }

        public void PauseOff()
        {
            set(false);
        }

        private void set(bool value)
        {
            simEventController.Set(simEvent, value);
        }

        public void Initialize()
        {
            simEventController.RegisterEvent(simEvent);
        }
    }
}
