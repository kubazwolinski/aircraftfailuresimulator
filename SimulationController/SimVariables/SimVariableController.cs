﻿using Microsoft.FlightSimulator.SimConnect;
using SimConnectService;
using System.Runtime.InteropServices;

namespace SimulationController.SimVariables
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    struct IntegerStruct
    {
        public int value;
    };

    public class SimVariableController
    {
        private readonly SimConnectWrapper wrapper;

        public SimVariableController(SimConnectWrapper wrapper)
        {
            this.wrapper = wrapper;
        }

        public void RegisterVariable(SimVariable simVariable)
        {
            wrapper.registerVariable<IntegerStruct>(simVariable.SimVariableEnum, simVariable.EventID, "", SIMCONNECT_DATATYPE.INT32);
        }

        public void Set(SimVariable simVariable, bool value)
        {
            wrapper.setVariable(simVariable.SimVariableEnum, new IntegerStruct { value = (value ? 1 : 0) });
        }
    }
}
