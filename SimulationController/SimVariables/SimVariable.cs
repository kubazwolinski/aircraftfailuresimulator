﻿namespace SimulationController.SimVariables
{
    public class SimVariable
    {
        public SimVariableEnum SimVariableEnum { get; private set; }

        public string EventID { get { return SimVariableEnum.ToString(); } }

        public SimVariable(SimVariableEnum simVariableEnum)
        {
            SimVariableEnum = simVariableEnum;
        }
    }
}
