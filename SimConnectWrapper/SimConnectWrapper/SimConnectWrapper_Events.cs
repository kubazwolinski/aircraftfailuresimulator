﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.FlightSimulator.SimConnect;
using System.Runtime.InteropServices;

namespace SimConnectService
{
    public partial class SimConnectWrapper
    { 
        public void mapClientToSimEvent(Enum clientEventEnum, string simEventId)
        {
            ensureConnected();

            simconnect.MapClientEventToSimEvent(clientEventEnum, simEventId);
        }

        public void transmitClientEvent(Enum clientEventEnum, uint value, Enum groupId)
        {
            ensureConnected();

            simconnect.TransmitClientEvent(0, clientEventEnum, value, groupId, SIMCONNECT_EVENT_FLAG.GROUPID_IS_PRIORITY);
        }

    }
}
