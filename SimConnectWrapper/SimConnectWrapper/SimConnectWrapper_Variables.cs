﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.FlightSimulator.SimConnect;
using System.Runtime.InteropServices;

namespace SimConnectService
{
    public partial class SimConnectWrapper
    { 
        public void registerVariable<T>(Enum defineId, string variableId, string unit, SIMCONNECT_DATATYPE dataType )
        {
            ensureConnected();

            simconnect.AddToDataDefinition(defineId, "Plane Altitude", "feet", dataType, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.RegisterDataDefineStruct<T>(defineId);
        }

        public void setVariable(Enum defineId, object structure)
        {
            ensureConnected();

            simconnect.SetDataOnSimObject(defineId, SimConnect.SIMCONNECT_OBJECT_ID_USER, SIMCONNECT_DATA_SET_FLAG.DEFAULT, structure);
        }
    }
}
