﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.FlightSimulator.SimConnect;
using System.Runtime.InteropServices;

namespace SimConnectService
{
    public partial class SimConnectWrapper
    {
        private const int WM_USER_SIMCONNECT = 0x0402;

        private SimConnect simconnect = null;

        public void connect(string name, IntPtr handle)
        {
            ensureDisonnected();
            simconnect = new SimConnect(name, handle, WM_USER_SIMCONNECT, null, 0);
        }

        public void disconnect()
        {
            ensureConnected();
            simconnect.Dispose();
            simconnect = null;
        }

        public bool Connected { get { return simconnect != null; } }

        protected void ensureConnected()
        {
            if (!Connected)
            {
                throw new InvalidOperationException();
            }
        }

        protected void ensureDisonnected()
        {
            if (Connected)
            {
                throw new InvalidOperationException();
            }
        }
    }
}
