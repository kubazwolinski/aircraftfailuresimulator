﻿using SimConnectService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SimulationController;
using SimulationController.SimEvents;
using SimulationController.Failures;
using SimulationController.SimVariables;

namespace fsx_gui
{
    public partial class MainWindow : Form
    {
        SimulationController.SimulationController simulationController = new SimulationController.SimulationController();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            simulationController.connect(this.Handle);
            initializeFailureCheckboxes();
            simulationController.PauseController.Initialize();

            unpauseIfNeeded();

            connectButton.Enabled = false;
            disconnectButton.Enabled = true;
            manualFailuresPanel.Enabled = true;
        }

        private void disconnectButton_Click(object sender, EventArgs e)
        {
            simulationController.disconnect();
            connectButton.Enabled = true;
            disconnectButton.Enabled = false;
            manualFailuresPanel.Enabled = false;
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            disconnectButton.PerformClick();
        }

        private void failureCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox senderCheckBox = sender as CheckBox;
            Failure failure = senderCheckBox.Tag as Failure;
            failure.Set(senderCheckBox.Checked);
        }

        private void unpause()
        {
            simulationController.PauseController.PauseOff();
        }

        private void unpauseIfNeeded()
        {
            if (unpauseCheckbox.Checked)
            {
                unpause();
            }
        }

        private void initializeFailureCheckboxes()
        {
            engine1FailureCheckBox.Tag = simulationController.FailureFactory.Create(SimEventEnum.TOGGLE_ENGINE1_FAILURE);
            engine2FailureCheckBox.Tag = simulationController.FailureFactory.Create(SimEventEnum.TOGGLE_ENGINE2_FAILURE);
            engine3FailureCheckBox.Tag = simulationController.FailureFactory.Create(SimEventEnum.TOGGLE_ENGINE3_FAILURE);
            engine4FailureCheckBox.Tag = simulationController.FailureFactory.Create(SimEventEnum.TOGGLE_ENGINE4_FAILURE);
            toggleVacuumFailureCheckBox.Tag = simulationController.FailureFactory.Create(SimEventEnum.TOGGLE_VACUUM_FAILURE);
            toggleElectricalFailureCheckBox.Tag = simulationController.FailureFactory.Create(SimEventEnum.TOGGLE_ELECTRICAL_FAILURE);
            togglePitotBlockageCheckbox.Tag = simulationController.FailureFactory.Create(SimEventEnum.TOGGLE_PITOT_BLOCKAGE);
            toggleStaticPortBlockageCheckBox.Tag = simulationController.FailureFactory.Create(SimEventEnum.TOGGLE_STATIC_PORT_BLOCKAGE);
            toggleHydraulicFailureCheckBox.Tag = simulationController.FailureFactory.Create(SimEventEnum.TOGGLE_HYDRAULIC_FAILURE);
            toggleLeftBreakFailureCheckBox.Tag = simulationController.FailureFactory.Create(SimEventEnum.TOGGLE_LEFT_BRAKE_FAILURE);
            toggleTotalBreakFailureCheckBox.Tag = simulationController.FailureFactory.Create(SimEventEnum.TOGGLE_TOTAL_BRAKE_FAILURE);
            toggleRightBreakFailureCheckBox.Tag = simulationController.FailureFactory.Create(SimEventEnum.TOGGLE_RIGHT_BRAKE_FAILURE);

            panelAdfFailureCheckBox.Tag = simulationController.FailureFactory.Create(SimVariableEnum.PARTIAL_PANEL_ADF);
            panelAirspeedFailureCheckBox.Tag = simulationController.FailureFactory.Create(SimVariableEnum.PARTIAL_PANEL_AIRSPEED);
            panelAltimeterFailureCheckBox.Tag = simulationController.FailureFactory.Create(SimVariableEnum.PARTIAL_PANEL_ALTIMETER);
            panelAttitudeFailureCheckBox.Tag = simulationController.FailureFactory.Create(SimVariableEnum.PARTIAL_PANEL_ATTITUDE);
            panelCommFailureCheckBox.Tag = simulationController.FailureFactory.Create(SimVariableEnum.PARTIAL_PANEL_COMM);
            panelCompasFailureCheckBox.Tag = simulationController.FailureFactory.Create(SimVariableEnum.PARTIAL_PANEL_COMPAS);
            panelElectricalFailureCheckBox.Tag = simulationController.FailureFactory.Create(SimVariableEnum.PARTIAL_PANEL_ELECTRICAL);
            panelEngineFailureCheckBox.Tag = simulationController.FailureFactory.Create(SimVariableEnum.PARTIAL_PANEL_ENGINE);
            panelHeadingFailureCheckBox.Tag = simulationController.FailureFactory.Create(SimVariableEnum.PARTIAL_PANEL_HEADING);
            panelVerticalVelocityFailureCheckBox.Tag = simulationController.FailureFactory.Create(SimVariableEnum.PARTIAL_PANEL_VERTICAL_VELOCITY);
            panelTransponderFailureCheckBox.Tag = simulationController.FailureFactory.Create(SimVariableEnum.PARTIAL_PANEL_TRANSPONDER);
            panelNavFailureCheckBox.Tag = simulationController.FailureFactory.Create(SimVariableEnum.PARTIAL_PANEL_NAV);
            panelPitotFailureCheckBox.Tag = simulationController.FailureFactory.Create(SimVariableEnum.PARTIAL_PANEL_PITOT);
            panelVacuumFailureCheckBox.Tag = simulationController.FailureFactory.Create(SimVariableEnum.PARTIAL_PANEL_VACUUM);
        }
    }
}
