﻿namespace fsx_gui
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.connectButton = new System.Windows.Forms.Button();
            this.disconnectButton = new System.Windows.Forms.Button();
            this.manualFailuresPanel = new System.Windows.Forms.Panel();
            this.breakFailiureGroupBox = new System.Windows.Forms.GroupBox();
            this.toggleRightBreakFailureCheckBox = new System.Windows.Forms.CheckBox();
            this.toggleLeftBreakFailureCheckBox = new System.Windows.Forms.CheckBox();
            this.toggleTotalBreakFailureCheckBox = new System.Windows.Forms.CheckBox();
            this.engineFailureGroupBox = new System.Windows.Forms.GroupBox();
            this.engine4FailureCheckBox = new System.Windows.Forms.CheckBox();
            this.engine3FailureCheckBox = new System.Windows.Forms.CheckBox();
            this.engine2FailureCheckBox = new System.Windows.Forms.CheckBox();
            this.engine1FailureCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.toggleHydraulicFailureCheckBox = new System.Windows.Forms.CheckBox();
            this.toggleStaticPortBlockageCheckBox = new System.Windows.Forms.CheckBox();
            this.togglePitotBlockageCheckbox = new System.Windows.Forms.CheckBox();
            this.toggleElectricalFailureCheckBox = new System.Windows.Forms.CheckBox();
            this.toggleVacuumFailureCheckBox = new System.Windows.Forms.CheckBox();
            this.panelFailureGroupBox = new System.Windows.Forms.GroupBox();
            this.panelVacuumFailureCheckBox = new System.Windows.Forms.CheckBox();
            this.panelPitotFailureCheckBox = new System.Windows.Forms.CheckBox();
            this.panelNavFailureCheckBox = new System.Windows.Forms.CheckBox();
            this.panelTransponderFailureCheckBox = new System.Windows.Forms.CheckBox();
            this.panelVerticalVelocityFailureCheckBox = new System.Windows.Forms.CheckBox();
            this.panelHeadingFailureCheckBox = new System.Windows.Forms.CheckBox();
            this.panelEngineFailureCheckBox = new System.Windows.Forms.CheckBox();
            this.panelElectricalFailureCheckBox = new System.Windows.Forms.CheckBox();
            this.panelCompasFailureCheckBox = new System.Windows.Forms.CheckBox();
            this.panelCommFailureCheckBox = new System.Windows.Forms.CheckBox();
            this.panelAttitudeFailureCheckBox = new System.Windows.Forms.CheckBox();
            this.panelAltimeterFailureCheckBox = new System.Windows.Forms.CheckBox();
            this.panelAirspeedFailureCheckBox = new System.Windows.Forms.CheckBox();
            this.panelAdfFailureCheckBox = new System.Windows.Forms.CheckBox();
            this.unpauseCheckbox = new System.Windows.Forms.CheckBox();
            this.manualFailuresPanel.SuspendLayout();
            this.breakFailiureGroupBox.SuspendLayout();
            this.engineFailureGroupBox.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panelFailureGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(11, 15);
            this.connectButton.Margin = new System.Windows.Forms.Padding(2);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(136, 28);
            this.connectButton.TabIndex = 5;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // disconnectButton
            // 
            this.disconnectButton.Enabled = false;
            this.disconnectButton.Location = new System.Drawing.Point(11, 47);
            this.disconnectButton.Margin = new System.Windows.Forms.Padding(2);
            this.disconnectButton.Name = "disconnectButton";
            this.disconnectButton.Size = new System.Drawing.Size(136, 28);
            this.disconnectButton.TabIndex = 6;
            this.disconnectButton.Text = "Disconnect";
            this.disconnectButton.UseVisualStyleBackColor = true;
            this.disconnectButton.Click += new System.EventHandler(this.disconnectButton_Click);
            // 
            // manualFailuresPanel
            // 
            this.manualFailuresPanel.Controls.Add(this.breakFailiureGroupBox);
            this.manualFailuresPanel.Controls.Add(this.engineFailureGroupBox);
            this.manualFailuresPanel.Controls.Add(this.groupBox2);
            this.manualFailuresPanel.Controls.Add(this.panelFailureGroupBox);
            this.manualFailuresPanel.Enabled = false;
            this.manualFailuresPanel.Location = new System.Drawing.Point(152, 15);
            this.manualFailuresPanel.Name = "manualFailuresPanel";
            this.manualFailuresPanel.Size = new System.Drawing.Size(465, 365);
            this.manualFailuresPanel.TabIndex = 7;
            // 
            // breakFailiureGroupBox
            // 
            this.breakFailiureGroupBox.Controls.Add(this.toggleRightBreakFailureCheckBox);
            this.breakFailiureGroupBox.Controls.Add(this.toggleLeftBreakFailureCheckBox);
            this.breakFailiureGroupBox.Controls.Add(this.toggleTotalBreakFailureCheckBox);
            this.breakFailiureGroupBox.Location = new System.Drawing.Point(209, 277);
            this.breakFailiureGroupBox.Name = "breakFailiureGroupBox";
            this.breakFailiureGroupBox.Size = new System.Drawing.Size(246, 52);
            this.breakFailiureGroupBox.TabIndex = 6;
            this.breakFailiureGroupBox.TabStop = false;
            this.breakFailiureGroupBox.Text = "BREAK FAILURE";
            // 
            // toggleRightBreakFailureCheckBox
            // 
            this.toggleRightBreakFailureCheckBox.AutoSize = true;
            this.toggleRightBreakFailureCheckBox.Location = new System.Drawing.Point(132, 23);
            this.toggleRightBreakFailureCheckBox.Name = "toggleRightBreakFailureCheckBox";
            this.toggleRightBreakFailureCheckBox.Size = new System.Drawing.Size(60, 17);
            this.toggleRightBreakFailureCheckBox.TabIndex = 1;
            this.toggleRightBreakFailureCheckBox.Text = "RIGHT";
            this.toggleRightBreakFailureCheckBox.UseVisualStyleBackColor = true;
            this.toggleRightBreakFailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // toggleLeftBreakFailureCheckBox
            // 
            this.toggleLeftBreakFailureCheckBox.AutoSize = true;
            this.toggleLeftBreakFailureCheckBox.Location = new System.Drawing.Point(7, 23);
            this.toggleLeftBreakFailureCheckBox.Name = "toggleLeftBreakFailureCheckBox";
            this.toggleLeftBreakFailureCheckBox.Size = new System.Drawing.Size(52, 17);
            this.toggleLeftBreakFailureCheckBox.TabIndex = 2;
            this.toggleLeftBreakFailureCheckBox.Text = "LEFT";
            this.toggleLeftBreakFailureCheckBox.UseVisualStyleBackColor = true;
            this.toggleLeftBreakFailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // toggleTotalBreakFailureCheckBox
            // 
            this.toggleTotalBreakFailureCheckBox.AutoSize = true;
            this.toggleTotalBreakFailureCheckBox.Location = new System.Drawing.Point(65, 23);
            this.toggleTotalBreakFailureCheckBox.Name = "toggleTotalBreakFailureCheckBox";
            this.toggleTotalBreakFailureCheckBox.Size = new System.Drawing.Size(61, 17);
            this.toggleTotalBreakFailureCheckBox.TabIndex = 3;
            this.toggleTotalBreakFailureCheckBox.Text = "TOTAL";
            this.toggleTotalBreakFailureCheckBox.UseVisualStyleBackColor = true;
            this.toggleTotalBreakFailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // engineFailureGroupBox
            // 
            this.engineFailureGroupBox.Controls.Add(this.engine4FailureCheckBox);
            this.engineFailureGroupBox.Controls.Add(this.engine3FailureCheckBox);
            this.engineFailureGroupBox.Controls.Add(this.engine2FailureCheckBox);
            this.engineFailureGroupBox.Controls.Add(this.engine1FailureCheckBox);
            this.engineFailureGroupBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.engineFailureGroupBox.Location = new System.Drawing.Point(209, 156);
            this.engineFailureGroupBox.Name = "engineFailureGroupBox";
            this.engineFailureGroupBox.Size = new System.Drawing.Size(246, 115);
            this.engineFailureGroupBox.TabIndex = 5;
            this.engineFailureGroupBox.TabStop = false;
            this.engineFailureGroupBox.Text = "ENGINE FAILURE";
            // 
            // engine4FailureCheckBox
            // 
            this.engine4FailureCheckBox.AutoSize = true;
            this.engine4FailureCheckBox.Location = new System.Drawing.Point(7, 91);
            this.engine4FailureCheckBox.Name = "engine4FailureCheckBox";
            this.engine4FailureCheckBox.Size = new System.Drawing.Size(76, 17);
            this.engine4FailureCheckBox.TabIndex = 3;
            this.engine4FailureCheckBox.Text = "ENGINE 4";
            this.engine4FailureCheckBox.UseVisualStyleBackColor = true;
            this.engine4FailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // engine3FailureCheckBox
            // 
            this.engine3FailureCheckBox.AutoSize = true;
            this.engine3FailureCheckBox.Location = new System.Drawing.Point(7, 67);
            this.engine3FailureCheckBox.Name = "engine3FailureCheckBox";
            this.engine3FailureCheckBox.Size = new System.Drawing.Size(76, 17);
            this.engine3FailureCheckBox.TabIndex = 2;
            this.engine3FailureCheckBox.Text = "ENGINE 3";
            this.engine3FailureCheckBox.UseVisualStyleBackColor = true;
            this.engine3FailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // engine2FailureCheckBox
            // 
            this.engine2FailureCheckBox.AutoSize = true;
            this.engine2FailureCheckBox.Location = new System.Drawing.Point(7, 43);
            this.engine2FailureCheckBox.Name = "engine2FailureCheckBox";
            this.engine2FailureCheckBox.Size = new System.Drawing.Size(76, 17);
            this.engine2FailureCheckBox.TabIndex = 1;
            this.engine2FailureCheckBox.Text = "ENGINE 2";
            this.engine2FailureCheckBox.UseVisualStyleBackColor = true;
            this.engine2FailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // engine1FailureCheckBox
            // 
            this.engine1FailureCheckBox.AutoSize = true;
            this.engine1FailureCheckBox.Location = new System.Drawing.Point(7, 20);
            this.engine1FailureCheckBox.Name = "engine1FailureCheckBox";
            this.engine1FailureCheckBox.Size = new System.Drawing.Size(76, 17);
            this.engine1FailureCheckBox.TabIndex = 0;
            this.engine1FailureCheckBox.Text = "ENGINE 1";
            this.engine1FailureCheckBox.UseVisualStyleBackColor = true;
            this.engine1FailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.toggleHydraulicFailureCheckBox);
            this.groupBox2.Controls.Add(this.toggleStaticPortBlockageCheckBox);
            this.groupBox2.Controls.Add(this.togglePitotBlockageCheckbox);
            this.groupBox2.Controls.Add(this.toggleElectricalFailureCheckBox);
            this.groupBox2.Controls.Add(this.toggleVacuumFailureCheckBox);
            this.groupBox2.Location = new System.Drawing.Point(209, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(246, 147);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "AIRCRAFT FAILURE";
            // 
            // toggleHydraulicFailureCheckBox
            // 
            this.toggleHydraulicFailureCheckBox.AutoSize = true;
            this.toggleHydraulicFailureCheckBox.Location = new System.Drawing.Point(7, 112);
            this.toggleHydraulicFailureCheckBox.Name = "toggleHydraulicFailureCheckBox";
            this.toggleHydraulicFailureCheckBox.Size = new System.Drawing.Size(178, 17);
            this.toggleHydraulicFailureCheckBox.TabIndex = 0;
            this.toggleHydraulicFailureCheckBox.Text = "Toggles hydraulic system failure \t";
            this.toggleHydraulicFailureCheckBox.UseVisualStyleBackColor = true;
            this.toggleHydraulicFailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // toggleStaticPortBlockageCheckBox
            // 
            this.toggleStaticPortBlockageCheckBox.AutoSize = true;
            this.toggleStaticPortBlockageCheckBox.Location = new System.Drawing.Point(7, 89);
            this.toggleStaticPortBlockageCheckBox.Name = "toggleStaticPortBlockageCheckBox";
            this.toggleStaticPortBlockageCheckBox.Size = new System.Drawing.Size(154, 17);
            this.toggleStaticPortBlockageCheckBox.TabIndex = 0;
            this.toggleStaticPortBlockageCheckBox.Text = "Toggles blocked static port";
            this.toggleStaticPortBlockageCheckBox.UseVisualStyleBackColor = true;
            this.toggleStaticPortBlockageCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // togglePitotBlockageCheckbox
            // 
            this.togglePitotBlockageCheckbox.AutoSize = true;
            this.togglePitotBlockageCheckbox.Location = new System.Drawing.Point(7, 66);
            this.togglePitotBlockageCheckbox.Name = "togglePitotBlockageCheckbox";
            this.togglePitotBlockageCheckbox.Size = new System.Drawing.Size(152, 17);
            this.togglePitotBlockageCheckbox.TabIndex = 0;
            this.togglePitotBlockageCheckbox.Text = "Toggles blocked pitot tube";
            this.togglePitotBlockageCheckbox.UseVisualStyleBackColor = true;
            this.togglePitotBlockageCheckbox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // toggleElectricalFailureCheckBox
            // 
            this.toggleElectricalFailureCheckBox.AutoSize = true;
            this.toggleElectricalFailureCheckBox.Location = new System.Drawing.Point(7, 43);
            this.toggleElectricalFailureCheckBox.Name = "toggleElectricalFailureCheckBox";
            this.toggleElectricalFailureCheckBox.Size = new System.Drawing.Size(170, 17);
            this.toggleElectricalFailureCheckBox.TabIndex = 0;
            this.toggleElectricalFailureCheckBox.Text = "Toggle electrical system failure";
            this.toggleElectricalFailureCheckBox.UseVisualStyleBackColor = true;
            this.toggleElectricalFailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // toggleVacuumFailureCheckBox
            // 
            this.toggleVacuumFailureCheckBox.AutoSize = true;
            this.toggleVacuumFailureCheckBox.Location = new System.Drawing.Point(7, 20);
            this.toggleVacuumFailureCheckBox.Name = "toggleVacuumFailureCheckBox";
            this.toggleVacuumFailureCheckBox.Size = new System.Drawing.Size(166, 17);
            this.toggleVacuumFailureCheckBox.TabIndex = 0;
            this.toggleVacuumFailureCheckBox.Text = "Toggle vacuum system failure";
            this.toggleVacuumFailureCheckBox.UseVisualStyleBackColor = true;
            this.toggleVacuumFailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // panelFailureGroupBox
            // 
            this.panelFailureGroupBox.Controls.Add(this.panelVacuumFailureCheckBox);
            this.panelFailureGroupBox.Controls.Add(this.panelPitotFailureCheckBox);
            this.panelFailureGroupBox.Controls.Add(this.panelNavFailureCheckBox);
            this.panelFailureGroupBox.Controls.Add(this.panelTransponderFailureCheckBox);
            this.panelFailureGroupBox.Controls.Add(this.panelVerticalVelocityFailureCheckBox);
            this.panelFailureGroupBox.Controls.Add(this.panelHeadingFailureCheckBox);
            this.panelFailureGroupBox.Controls.Add(this.panelEngineFailureCheckBox);
            this.panelFailureGroupBox.Controls.Add(this.panelElectricalFailureCheckBox);
            this.panelFailureGroupBox.Controls.Add(this.panelCompasFailureCheckBox);
            this.panelFailureGroupBox.Controls.Add(this.panelCommFailureCheckBox);
            this.panelFailureGroupBox.Controls.Add(this.panelAttitudeFailureCheckBox);
            this.panelFailureGroupBox.Controls.Add(this.panelAltimeterFailureCheckBox);
            this.panelFailureGroupBox.Controls.Add(this.panelAirspeedFailureCheckBox);
            this.panelFailureGroupBox.Controls.Add(this.panelAdfFailureCheckBox);
            this.panelFailureGroupBox.Location = new System.Drawing.Point(3, 3);
            this.panelFailureGroupBox.Name = "panelFailureGroupBox";
            this.panelFailureGroupBox.Size = new System.Drawing.Size(200, 352);
            this.panelFailureGroupBox.TabIndex = 3;
            this.panelFailureGroupBox.TabStop = false;
            this.panelFailureGroupBox.Text = "PANEL FAILURE";
            // 
            // panelVacuumFailureCheckBox
            // 
            this.panelVacuumFailureCheckBox.AutoSize = true;
            this.panelVacuumFailureCheckBox.Location = new System.Drawing.Point(5, 320);
            this.panelVacuumFailureCheckBox.Name = "panelVacuumFailureCheckBox";
            this.panelVacuumFailureCheckBox.Size = new System.Drawing.Size(110, 17);
            this.panelVacuumFailureCheckBox.TabIndex = 2;
            this.panelVacuumFailureCheckBox.Text = "PANEL VACUUM";
            this.panelVacuumFailureCheckBox.UseVisualStyleBackColor = true;
            this.panelVacuumFailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // panelPitotFailureCheckBox
            // 
            this.panelPitotFailureCheckBox.AutoSize = true;
            this.panelPitotFailureCheckBox.Location = new System.Drawing.Point(6, 297);
            this.panelPitotFailureCheckBox.Name = "panelPitotFailureCheckBox";
            this.panelPitotFailureCheckBox.Size = new System.Drawing.Size(96, 17);
            this.panelPitotFailureCheckBox.TabIndex = 2;
            this.panelPitotFailureCheckBox.Text = "PANEL PITOT";
            this.panelPitotFailureCheckBox.UseVisualStyleBackColor = true;
            this.panelPitotFailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // panelNavFailureCheckBox
            // 
            this.panelNavFailureCheckBox.AutoSize = true;
            this.panelNavFailureCheckBox.Location = new System.Drawing.Point(6, 274);
            this.panelNavFailureCheckBox.Name = "panelNavFailureCheckBox";
            this.panelNavFailureCheckBox.Size = new System.Drawing.Size(86, 17);
            this.panelNavFailureCheckBox.TabIndex = 2;
            this.panelNavFailureCheckBox.Text = "PANEL NAV";
            this.panelNavFailureCheckBox.UseVisualStyleBackColor = true;
            this.panelNavFailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // panelTransponderFailureCheckBox
            // 
            this.panelTransponderFailureCheckBox.AutoSize = true;
            this.panelTransponderFailureCheckBox.Location = new System.Drawing.Point(6, 251);
            this.panelTransponderFailureCheckBox.Name = "panelTransponderFailureCheckBox";
            this.panelTransponderFailureCheckBox.Size = new System.Drawing.Size(147, 17);
            this.panelTransponderFailureCheckBox.TabIndex = 2;
            this.panelTransponderFailureCheckBox.Text = "PANEL TRANSPONDER";
            this.panelTransponderFailureCheckBox.UseVisualStyleBackColor = true;
            this.panelTransponderFailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // panelVerticalVelocityFailureCheckBox
            // 
            this.panelVerticalVelocityFailureCheckBox.AutoSize = true;
            this.panelVerticalVelocityFailureCheckBox.Location = new System.Drawing.Point(6, 228);
            this.panelVerticalVelocityFailureCheckBox.Name = "panelVerticalVelocityFailureCheckBox";
            this.panelVerticalVelocityFailureCheckBox.Size = new System.Drawing.Size(171, 17);
            this.panelVerticalVelocityFailureCheckBox.TabIndex = 2;
            this.panelVerticalVelocityFailureCheckBox.Text = "PANEL VERTICAL VELOCITY";
            this.panelVerticalVelocityFailureCheckBox.UseVisualStyleBackColor = true;
            this.panelVerticalVelocityFailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // panelHeadingFailureCheckBox
            // 
            this.panelHeadingFailureCheckBox.AutoSize = true;
            this.panelHeadingFailureCheckBox.Location = new System.Drawing.Point(6, 205);
            this.panelHeadingFailureCheckBox.Name = "panelHeadingFailureCheckBox";
            this.panelHeadingFailureCheckBox.Size = new System.Drawing.Size(113, 17);
            this.panelHeadingFailureCheckBox.TabIndex = 2;
            this.panelHeadingFailureCheckBox.Text = "PANEL HEADING";
            this.panelHeadingFailureCheckBox.UseVisualStyleBackColor = true;
            this.panelHeadingFailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // panelEngineFailureCheckBox
            // 
            this.panelEngineFailureCheckBox.AutoSize = true;
            this.panelEngineFailureCheckBox.Location = new System.Drawing.Point(6, 182);
            this.panelEngineFailureCheckBox.Name = "panelEngineFailureCheckBox";
            this.panelEngineFailureCheckBox.Size = new System.Drawing.Size(105, 17);
            this.panelEngineFailureCheckBox.TabIndex = 2;
            this.panelEngineFailureCheckBox.Text = "PANEL ENGINE";
            this.panelEngineFailureCheckBox.UseVisualStyleBackColor = true;
            this.panelEngineFailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // panelElectricalFailureCheckBox
            // 
            this.panelElectricalFailureCheckBox.AutoSize = true;
            this.panelElectricalFailureCheckBox.Location = new System.Drawing.Point(6, 159);
            this.panelElectricalFailureCheckBox.Name = "panelElectricalFailureCheckBox";
            this.panelElectricalFailureCheckBox.Size = new System.Drawing.Size(129, 17);
            this.panelElectricalFailureCheckBox.TabIndex = 2;
            this.panelElectricalFailureCheckBox.Text = "PANEL ELECTRICAL";
            this.panelElectricalFailureCheckBox.UseVisualStyleBackColor = true;
            this.panelElectricalFailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // panelCompasFailureCheckBox
            // 
            this.panelCompasFailureCheckBox.AutoSize = true;
            this.panelCompasFailureCheckBox.Location = new System.Drawing.Point(6, 136);
            this.panelCompasFailureCheckBox.Name = "panelCompasFailureCheckBox";
            this.panelCompasFailureCheckBox.Size = new System.Drawing.Size(109, 17);
            this.panelCompasFailureCheckBox.TabIndex = 2;
            this.panelCompasFailureCheckBox.Text = "PANEL COMPAS";
            this.panelCompasFailureCheckBox.UseVisualStyleBackColor = true;
            this.panelCompasFailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // panelCommFailureCheckBox
            // 
            this.panelCommFailureCheckBox.AutoSize = true;
            this.panelCommFailureCheckBox.Location = new System.Drawing.Point(6, 113);
            this.panelCommFailureCheckBox.Name = "panelCommFailureCheckBox";
            this.panelCommFailureCheckBox.Size = new System.Drawing.Size(97, 17);
            this.panelCommFailureCheckBox.TabIndex = 2;
            this.panelCommFailureCheckBox.Text = "PANEL COMM";
            this.panelCommFailureCheckBox.UseVisualStyleBackColor = true;
            this.panelCommFailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // panelAttitudeFailureCheckBox
            // 
            this.panelAttitudeFailureCheckBox.AutoSize = true;
            this.panelAttitudeFailureCheckBox.Location = new System.Drawing.Point(6, 90);
            this.panelAttitudeFailureCheckBox.Name = "panelAttitudeFailureCheckBox";
            this.panelAttitudeFailureCheckBox.Size = new System.Drawing.Size(118, 17);
            this.panelAttitudeFailureCheckBox.TabIndex = 2;
            this.panelAttitudeFailureCheckBox.Text = "PANEL ATTITUDE";
            this.panelAttitudeFailureCheckBox.UseVisualStyleBackColor = true;
            this.panelAttitudeFailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // panelAltimeterFailureCheckBox
            // 
            this.panelAltimeterFailureCheckBox.AutoSize = true;
            this.panelAltimeterFailureCheckBox.Location = new System.Drawing.Point(6, 67);
            this.panelAltimeterFailureCheckBox.Name = "panelAltimeterFailureCheckBox";
            this.panelAltimeterFailureCheckBox.Size = new System.Drawing.Size(125, 17);
            this.panelAltimeterFailureCheckBox.TabIndex = 2;
            this.panelAltimeterFailureCheckBox.Text = "PANEL ALTIMETER";
            this.panelAltimeterFailureCheckBox.UseVisualStyleBackColor = true;
            this.panelAltimeterFailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // panelAirspeedFailureCheckBox
            // 
            this.panelAirspeedFailureCheckBox.AutoSize = true;
            this.panelAirspeedFailureCheckBox.Location = new System.Drawing.Point(7, 44);
            this.panelAirspeedFailureCheckBox.Name = "panelAirspeedFailureCheckBox";
            this.panelAirspeedFailureCheckBox.Size = new System.Drawing.Size(118, 17);
            this.panelAirspeedFailureCheckBox.TabIndex = 2;
            this.panelAirspeedFailureCheckBox.Text = "PANEL AIRSPEED";
            this.panelAirspeedFailureCheckBox.UseVisualStyleBackColor = true;
            this.panelAirspeedFailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // panelAdfFailureCheckBox
            // 
            this.panelAdfFailureCheckBox.AutoSize = true;
            this.panelAdfFailureCheckBox.Location = new System.Drawing.Point(7, 20);
            this.panelAdfFailureCheckBox.Name = "panelAdfFailureCheckBox";
            this.panelAdfFailureCheckBox.Size = new System.Drawing.Size(85, 17);
            this.panelAdfFailureCheckBox.TabIndex = 1;
            this.panelAdfFailureCheckBox.Text = "PANEL ADF";
            this.panelAdfFailureCheckBox.UseVisualStyleBackColor = true;
            this.panelAdfFailureCheckBox.CheckedChanged += new System.EventHandler(this.failureCheckBox_CheckedChanged);
            // 
            // unpauseCheckbox
            // 
            this.unpauseCheckbox.AutoSize = true;
            this.unpauseCheckbox.Checked = true;
            this.unpauseCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.unpauseCheckbox.Location = new System.Drawing.Point(11, 82);
            this.unpauseCheckbox.Name = "unpauseCheckbox";
            this.unpauseCheckbox.Size = new System.Drawing.Size(125, 17);
            this.unpauseCheckbox.TabIndex = 8;
            this.unpauseCheckbox.Text = "Unpause after action";
            this.unpauseCheckbox.UseVisualStyleBackColor = true;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(621, 383);
            this.Controls.Add(this.unpauseCheckbox);
            this.Controls.Add(this.manualFailuresPanel);
            this.Controls.Add(this.disconnectButton);
            this.Controls.Add(this.connectButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.Text = "FailureSX";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainWindow_FormClosed);
            this.manualFailuresPanel.ResumeLayout(false);
            this.breakFailiureGroupBox.ResumeLayout(false);
            this.breakFailiureGroupBox.PerformLayout();
            this.engineFailureGroupBox.ResumeLayout(false);
            this.engineFailureGroupBox.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panelFailureGroupBox.ResumeLayout(false);
            this.panelFailureGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.Button disconnectButton;
        private System.Windows.Forms.Panel manualFailuresPanel;
        private System.Windows.Forms.GroupBox breakFailiureGroupBox;
        private System.Windows.Forms.CheckBox toggleRightBreakFailureCheckBox;
        private System.Windows.Forms.CheckBox toggleLeftBreakFailureCheckBox;
        private System.Windows.Forms.CheckBox toggleTotalBreakFailureCheckBox;
        private System.Windows.Forms.GroupBox engineFailureGroupBox;
        private System.Windows.Forms.CheckBox engine4FailureCheckBox;
        private System.Windows.Forms.CheckBox engine3FailureCheckBox;
        private System.Windows.Forms.CheckBox engine2FailureCheckBox;
        private System.Windows.Forms.CheckBox engine1FailureCheckBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox toggleHydraulicFailureCheckBox;
        private System.Windows.Forms.CheckBox toggleStaticPortBlockageCheckBox;
        private System.Windows.Forms.CheckBox togglePitotBlockageCheckbox;
        private System.Windows.Forms.CheckBox toggleElectricalFailureCheckBox;
        private System.Windows.Forms.CheckBox toggleVacuumFailureCheckBox;
        private System.Windows.Forms.GroupBox panelFailureGroupBox;
        private System.Windows.Forms.CheckBox panelVacuumFailureCheckBox;
        private System.Windows.Forms.CheckBox panelPitotFailureCheckBox;
        private System.Windows.Forms.CheckBox panelNavFailureCheckBox;
        private System.Windows.Forms.CheckBox panelTransponderFailureCheckBox;
        private System.Windows.Forms.CheckBox panelVerticalVelocityFailureCheckBox;
        private System.Windows.Forms.CheckBox panelHeadingFailureCheckBox;
        private System.Windows.Forms.CheckBox panelEngineFailureCheckBox;
        private System.Windows.Forms.CheckBox panelElectricalFailureCheckBox;
        private System.Windows.Forms.CheckBox panelCompasFailureCheckBox;
        private System.Windows.Forms.CheckBox panelCommFailureCheckBox;
        private System.Windows.Forms.CheckBox panelAttitudeFailureCheckBox;
        private System.Windows.Forms.CheckBox panelAltimeterFailureCheckBox;
        private System.Windows.Forms.CheckBox panelAirspeedFailureCheckBox;
        private System.Windows.Forms.CheckBox panelAdfFailureCheckBox;
        private System.Windows.Forms.CheckBox unpauseCheckbox;
    }
}

